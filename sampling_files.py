#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Apr 2021

@authors: marie-claire.blache@inrae.fr, benoit.piegu@cnrs.fr

sampling_files is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.
 *
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 *
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
"""

from pathlib import Path
import shutil
import random
import argparse
import os
import sys
import fnmatch
from collections import defaultdict
import re
import logging
import errno
from pprint import pformat

def samplingfiles(lfiles, n, sep, dir):

    # liste de listes à partir
    # du split du nom avec sep avec au max deux éléments
    # ne garde que le premier (le "film") & y ajoute le nom du fichier complet
    # exemple : ['film01_01.png', 'film01_02.png'] donne [['film01', 'film01_01.png'], ['film01', 'film01_02.png']]
    lfilm_img = [(f.split(sep, 1)[0], f) for f in lfiles]
    # regroupe les images par film
    dictimg = defaultdict(list)
    for film, img in lfilm_img:
        dictimg[film].append(img)
    logging.debug("  dictimg: \n%s", pformat(dictimg))
    # dictionnaire nombre d'images par film
    ## boucle permettant de creer une liste avec les nom de film
    #- for path in lfiles :
    #-    film_name = path.name.split("_")[0]
    #-    film_name_list.append(film_name)

    # dictionnaire avec le nombre d'images par film
    #-dictnimg = {i:film_name_list.count(i) for i in film_name_list}
    dictnimg = { film:len(limg) for film, limg in dictimg.items()}
    # ordonne le dictionnaire du nb d'image par film par le nb d'images
    dictnimg = {k: v for k, v in sorted(dictnimg.items(), key=lambda item: item[1])}
    logging.debug("  dictnimg: \n%s", pformat(dictnimg))

    # nombre de films présent
    nbre_film = len(dictnimg)

    nbimgparfilm = n // nbre_film
    logging.debug("  nfilm: %i --  n image / film : %i", nbre_film, nbimgparfilm)

    nbreimagerestant = n

    ## boucle permettant de déterminer le nombre d'images par film
    dictnimg2keep = {}
    for i, film in enumerate(dictnimg.keys()):
        if dictnimg[film] <= nbimgparfilm:
            dictnimg2keep[film] = dictnimg[film]
        else:
            dictnimg2keep[film] = nbimgparfilm

        nbreimagerestant = nbreimagerestant - dictnimg2keep[film]
        if(nbre_film - (i+1)) != 0:
            nbimgparfilm = nbreimagerestant // (nbre_film - (i+1))
    sys.stderr.write(
        "# {}: {} films -- {}/{} images to keep\n".
        format(dir, len(dictnimg2keep), sum(dictnimg2keep.values()), len(lfiles)))
    logging.info("  %s: %i films -- %i/%i images to keep",
                 dir, len(dictnimg2keep), sum(dictnimg2keep.values()), len(lfiles))
    logging.debug("  dictnimg2keep: %s", pformat(dictnimg2keep))

    # sampling
    img2keep = [random.sample(files, dictnimg2keep[film]) for film, files in dictimg.items() if len(files)>0]
    # import itertools
    # return  list(itertools.chain.from_iterable(list(itertools.chain())
    logging.debug("  img2keep: \n%s", pformat(img2keep))
    return [item for sublist in img2keep for item in sublist]


parser = argparse.ArgumentParser(description="""

Recursively sampling N (cf -n) files per directory and among base name (try to maximise represented
base names).  Put sampled files in <keep> directory). Base names are deduced from the firsts letters
before BASESEP character (cf --basesep). By default, reconstruct same tree in <keep> directory.
""",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--dir", "-d", default=".", help="dir where to execute the script")
parser.add_argument("--ext", "-e", required=True, action="append", help="glob to select file to sampling")
parser.add_argument("-n", type=int, default=30, help="number of files to sampling")
parser.add_argument("--link", action='store_true', help="make links on files rather than copying them")
parser.add_argument("--basesep", default="_", help="separator used to obtain base name")
parser.add_argument('--seed', type=int, default=42, help="seed for random")
# parser.add_argument("--dry-run", dest="no_dry_run", action='store_true',
#    help="dry run: don't create <keep> directories, don't copy/move files")
parser.add_argument("--no-dry-run", dest="no_dry_run", action='store_true',
                    help="no dry run: create <keep> directories and copy/move files")
parser.add_argument("--clean", action='store_true', help="clean <keep> directory before sampling files")
parser.add_argument('--debug', action='count', default=0, help="debug (levels are 0,1 and 2)")
parser.add_argument('--in_situ', action='store_true', help="create <keep> directory in each directory")
args = parser.parse_args()


# par défaut, ne fait rien
dry_run = False if args.no_dry_run else True
keep_dir_name = "keep"
symlinks = os.supports_follow_symlinks and args.link

log_level = logging.WARNING
if args.debug == 1:
    log_level = logging.INFO
elif args.debug >= 2:
    log_level = logging.DEBUG

logging.basicConfig(level=log_level,
                    format='%(asctime)s %(levelname)-8s [%(funcName)-14s] %(message)s',
                    datefmt='%m-%d %H:%M')

# with symlinks, dir path must be relative 
pathimg = Path(args.dir)
if symlinks and pathimg.is_absolute():
    print(f" dir <{args.dir}> is not a relative path! It must be with the option --link!")
    sys.exit(1)

random.seed(args.seed)
glob2sampling = ["*" + args.basesep + "*" + ext for ext in args.ext]
sys.stderr.write("# start from: <{}> -- glob=<{}>\n".format(args.dir, pformat(glob2sampling)))
# logging.debug("glob2sampling: \n%s", pformat(glob2sampling))
curdir = os.getcwd()
for curr_dir, dirs, files in os.walk(pathimg):
    pathdir = Path(curr_dir)
    if pathdir.name == keep_dir_name:
        continue
    if not len(files):
        continue
    # filters files with ext (and sep!)
    list2sample = [f for f in files if any([fnmatch.fnmatch(f, extglob) for extglob in glob2sampling])]
    logging.info("# dir = %s -- nfile = %i", curr_dir, len(list2sample))
    if not len(list2sample):
        continue
    files2sample = samplingfiles(list2sample, args.n, args.basesep, curr_dir)
    # créer le dossier
    pathkeep = pathdir / keep_dir_name if args.in_situ else pathimg / Path(keep_dir_name)
    logging.info("# pathkeep %s", pathkeep)
    if not Path.exists(pathkeep):
        logging.info("# creating '%s' directory", pathkeep)
        if not dry_run:
            try:
                os.makedirs(pathkeep, exist_ok=True)
            except OSError as err:
                if err.errno != errno.EEXIST:
                    logging.error("# can't create directory <%s> (%s)", pathkeep, err)
                    sys.exit(1)
    if not dry_run:
        # nettoyer si voulu
        if args.clean:
            # list of lisf of files to remove (by glob)
            lfiles2rm = [list(pathkeep.glob(extglob)) for extglob in glob2sampling]
            # use set to get unique element
            for f in set([str(item) for sublist in lfiles2rm for item in sublist]):
                logging.debug("# to del: %s\n", f)
                try:
                    os.remove(f)
                except OSError as err:
                    logging.error("# can't remove file <%s> in <%s> (%s)", f, pathkeep, err)

    for f in files2sample:
        src = pathdir / f
        dest = pathkeep / f
        if not args.in_situ:
            if symlinks:
                src = Path("../" * (1+len(list(Path(pathdir).parents)))) / pathdir / f
            else:
                src = pathdir / f
            dest = Path(keep_dir_name) / pathdir / f
        else:
            if symlinks:
                src = Path("..") / f
        logging.debug("# file keeped: %s -- src=%s dest=%s", pathdir / f, src, dest)
        if not dry_run:
            try:
                if symlinks:
                    os.symlink(src, dest)
                else:
                    shutil.copy(src, dest)
            except OSError as err:
                logging.error("# can't copy file <%s> in <%s> (%s)", pathdir / f, pathkeep, err)
