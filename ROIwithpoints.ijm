/* Marie-Claire BLACHE
 * Plateau d'imagerie Cellulaire PRC 
 * marie-claire.blache@inrae.fr
 * janvier 20223 english version
 * 
 * with ImageJ 1.54f version and java 1.8.0_265
 * 
 * The macro draws squares from manually positioned points (center of the square) for different areas (cheek, wattle, ear lobe, comb,white feathers, etc).  
 * 
 * Requirement:
 * 	- a folder containing the png images" 
 * 	
 * How to use:
 * 	- start the macro
 * 	- select the folder with the png images" 
 *
 *  ROIwithpoints is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
*/

/////// fonctions /////////////

//function to create a ROI based on point coordinates  
function pointsquare(size){
		
		// get points coordinates 
	 	getSelectionCoordinates(xCoordinates, yCoordinates);
		for(r=0; r<lengthOf(xCoordinates); r++) {
		//create a square with a fixed size
		x = xCoordinates[r] - (size/2); 
		y = yCoordinates[r] - (size/2); 
		makeRectangle(x,y,size,size);
		roiManager("Add");
			}}
//function to correct ROIs 
function check(name,size){			
		//check of ROIs
		roiManager("Show None");	
		roiManager("Show All");
		verifierROI = getBoolean("Are the " + name + " ROIs correct ? "); 
		while (verifierROI == 0){
			// get points coordinates
			waitForUser("Delete false ROIs and locate the new points for: " + name);
			type = selectionType;
			if (type == 10){
			pointsquare(size);
			roiManager("Show None");
			roiManager("Show All");
			verifierROI = getBoolean("Are the " + name + " ROIs correct ? ");
				}
			else {
			roiManager("Show None");
			roiManager("Show All");
			verifierROI = getBoolean("Are the " + name + " ROIs correct ? ");
				}
		}

		//name and merge ROIs
		count = roiManager("count");
		roi=0;
		for (roi=0; roi<count; roi++) {
			roiManager("Select", roi);
			nameroi = name + "_0" + (roi+1);
			roiManager("Rename", nameroi);
			listr = Array.concat(listr,roi); 
			}
		//merge ROIs	
		roiManager("Select", listr);
		roiManager("Combine");
		roiManager("Add");			
		
		//select the last ROIs 
		roiManager("select", count);
		roiManager("Rename", name + "_tot");
			 

		//save ROIs 
		roiManager("Deselect");
		roiManager("save", folderzip + name + "-ROIset.zip");	
		roiManager("Deselect");	
		roiManager("delete");
		roiManager("Show None");
	}	
	
		
// function to measure the ROIs average 
function MeanColor(img, roi){ 
	selectImage(img); 
	roiManager("Select", roi); 
	getStatistics(area, mean);
	return mean ;
	}

// function to create a text file with all the results
function txt(s1,s2,title1){
	title2 = "["+title1+"]";
	f = title2;
	if (isOpen(title1)){
		print(f, s2);
		}
	else {
	run("Text Window...", "name="+title2+" width=100 height=8 menu");
	print(f, s1 + s2); 
//	return 1
		}
	}   
//fonction to classify the feather region 
function featherimg(size){
	waitForUser("locate the points for feather");
	getSelectionCoordinates(xCoordinates, yCoordinates);
	x = xCoordinates[0] - (size/2); 
	y = yCoordinates[0] - (size/2); 
	makeRectangle(x,y,size,size);
	roiManager("Add");
 	Dialog.create("feather");
	items = newArray("Yes", "No", "I don't know");
	Dialog.addRadioButtonGroup("do you see the tip of the feathers ? ", items, 1, 3, "Non");
	Dialog.show;
	Hplume = Dialog.getRadioButton ;
	roiManager("Select", 0);
	roiManager("Rename", "feather");
	roiManager("save", folderzip + "feather.roi")
	run("Duplicate...", " ");
 	crop = getTitle;
 	saveAs("Tiff", feather + nameimg + "_" + Hplume); // 0 plume lisse 1 
 	close();
 	roiManager("Deselect");
	roiManager("Delete");
	return Hplume ;
	}

//function to select regions by name.
function findRoiWithName(roiName) { 
	nR = roiManager("Count"); 
 
	for (i=0; i<nR; i++) { 
		roiManager("Select", i); 
		rName = Roi.getName(); 
		if (matches(rName, roiName)) { 
			return i; 
		} 
	} 
	return -1; 
	} 

///////////////////////////////Start of the macro /////////////////////////

//Choice of folder with  raw data
dir = getDirectory("folder with png images");
list = getFileList(dir);

//create txt result folder 
sauve = dir + "resultsRGB" + File.separator;
File.makeDirectory(sauve);

//create feather image folder 
feather = dir + "feather" + File.separator;
File.makeDirectory(feather);

//create ROIs result folder
roi = dir + "roi" + File.separator;
File.makeDirectory(roi);

//select the multipoint tool  
setTool("multipoint");


for (i=0;i<list.length; i++) {
	path = dir + list[i];
	showProgress(i, list.length);
	//get image name 
	nameimg = File.getNameWithoutExtension(path); 
	//check path (to do not again the same image) 
	path_v = sauve + nameimg + ".txt"; 
	if (endsWith(path,".png") && File.exists(path_v) != 1){
		
		open(path); 
		selectImage(1);
		img = getTitle;  //raw data
		
		//create zip folder  
		folderzip= roi + nameimg + File.separator;
		File.makeDirectory(folderzip);
	
 		//Identify ROIs
 		zones = newArray("cheek", "wattle","ear lobe", "comb","white feathers");
		for (z=0; z<zones.length; z++){
		roiManager("Show None");	
		roiManager("Show All");
		waitForUser("locate the points for : " + zones[z]);
		type = selectionType; 
		while (type != 10){
 			waitForUser("problem with your tool, choose the multipoint tool for : " + zones[z]);
 			type = selectionType; 
				}
		pointsquare(10);
		check(zones[z],10); 
		}

		pluman = featherimg(120);
	
 		//create R, G and B images 
 		selectImage(img); 
 		run("Select All");
  		run("Duplicate...", " ");
		run("Split Channels"); 	
		selectImage(2);
   		R = getTitle;  //red
		selectImage(3);
		G = getTitle; //green
		selectImage(4);
   		B = getTitle;  //blue
		
   		//Open ROIs
   		listzip = getFileList(folderzip); 
 		for(zip=0;zip<listzip.length; zip++) { 
 			pathzip = folderzip + listzip[zip];
 			roiManager("Open", pathzip);
 			}
 		
 		//Check ROIs 	
 		roiManager("save", roi + nameimg + "_all_ROIset.zip") ;
		findRoiWithName("feather");
		roiManager("Delete");
			
	setBatchMode(true);
   		//for each image ROIs measurement
   	kroiallc1 = newArray("images") ;	
	vroiallc1 = newArray(img) ;
	kfin = newArray("feather", "\n"); 
	vfin = newArray(pluman, "\n"); 
   	for(r=0; r < roiManager("Count");  r++){
   		roiManager("select", r);
		nameroi = Roi.getName(); 
		red = MeanColor(R, r);
		green = MeanColor(G, r);
		blue = MeanColor(B, r);
		ratio = red / (red + green + blue); 
		print(ratio);
		kroiall = newArray( "ROI" , "red" , "green" , "blue", "ratio");
		kroiallc = Array.concat(kroiallc1, kroiall);		
		vroiall = newArray(nameroi, red, green, blue, ratio);
		vroiallc =  Array.concat(vroiallc1, vroiall);
		kroiallc = Array.concat(kroiallc, kfin);	
		vroiallc =  Array.concat(vroiallc, vfin);
		s1 = String.join(kroiallc,"\t"); 
		s2 = String.join(vroiallc, "\t"); 
		title1 = "RGB";
		txt(s1,s2,title1);
		nameroisplit = split(nameroi, "_"); 
		}		
		
		setBatchMode(false);
		roiManager("Deselect");
		roiManager("Delete");
		run("Close All");

		//save txt
		selectWindow("RGB");
		saveAs("txt", sauve  + nameimg); 
		run("Close");
 
		
		}}

waitForUser("Done");
   		