#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 15:06:34 2021

@author: marie-claire blache & Benoît Piegu
"""
############################import the necessary packages###############################

#import os
import pathlib
import random
import shutil

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications import ResNet50
from sklearn.metrics import classification_report
#from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse

########################################Options################################
default_debug = 0
default_epochs = 20
default_weights = 'imagenet'
default_iteration = 1
default_batch_size = 32
default_dynamique = 255
default_condition = 1


parser = argparse.ArgumentParser(description='')
parser.add_argument('--info', '-if', action='count',  default=0, help="info)")
group = parser.add_mutually_exclusive_group()
group.add_argument('--header', action='store_true', help="just prints the output header and quit")
parser.add_argument('--debug', '-db', type=int, dest="debug",
                     choices=[0,1], default=default_debug,
                     help="debug level (default: %(default)i)")
parser.add_argument("--epochs", '-ep', type=int, dest="epochs",
                     choices=range(1, 301), default=default_epochs,
                     help=" epoch parameter (default: %(default)i)")
parser.add_argument("--condition", '-cdt', type=int, dest="condition",
                     choices=range(0, 33), default=default_condition,
                     help="condition number (default: %(default)i)")
parser.add_argument("--iteration", '-it', type=int, dest="iteration",
                     choices=range(0, 33), default=default_iteration,
                     help="iteration number (default: %(default)i)")
parser.add_argument("--weights", '-wg', type=str, help="name of the weight selected. The default value is imagenet.", default= default_weights)
parser.add_argument("--batchsize", '-bs', type=int, dest="batchsize",
                     choices=range(0, 100), default=default_batch_size,
                     help="batch_size number (default: %(default)i)")
parser.add_argument("--dir", "-d", dest="dir", default=".", help="dir where to execute the script")
# parser.add_argument("-p", "--plot", type=str, default="plot.png", p="path to output loss/accuracy plot")
args = parser.parse_args()

################################Valeurs fixe#########################

IMG_SIZE = (224,224)
INIT_LR = 1e-4
CLASSES = ["other", "hen_profile"]

# grab the paths to all input images in the original input directory
# and shuffle them
folderPaths =pathlib.Path(args.dir)

TRAIN_PATH =  pathlib.Path.joinpath(folderPaths, "Train")
VAL_PATH = pathlib.Path.joinpath(folderPaths, "Val")
TEST_PATH =  pathlib.Path.joinpath(folderPaths, "Test")

totalTrain = len(list(TRAIN_PATH.glob('**/*.png')))
totalVal = len(list(VAL_PATH.glob('**/*.png')))
totalTest = len(list(TEST_PATH.glob('**/*.png')))

# initialize the training training data augmentation object
trainAug = ImageDataGenerator(
	rotation_range=25,
	zoom_range=0.1,
	width_shift_range=0.1,
	height_shift_range=0.1,
	shear_range=0.2,
	horizontal_flip=True,
	fill_mode="nearest")

# initialize the validation/testing data augmentation object (which
# we'll be adding mean subtraction to)
valAug = ImageDataGenerator()

# initialize the training generator
trainGen = trainAug.flow_from_directory(
	TRAIN_PATH,
	class_mode="categorical",
	target_size= IMG_SIZE,
	color_mode="rgb",
	shuffle=True,
	batch_size=args.batchsize)

# initialize the validation generator
valGen = valAug.flow_from_directory(
	VAL_PATH,
	class_mode="categorical",
	target_size= IMG_SIZE,
	color_mode="rgb",
	shuffle=False,
	batch_size=args.batchsize)

# initialize the testing generator
testGen = valAug.flow_from_directory(
	TEST_PATH,
	class_mode="categorical",
	target_size= IMG_SIZE,
	color_mode="rgb",
	shuffle=False,
	batch_size=args.batchsize)

# load the ResNet-50 network, ensuring the head FC layer sets are left
# off
print("[INFO] preparing model...")
baseModel = ResNet50(weights="imagenet", include_top=False,
	input_tensor=Input(shape=(224, 224, 3)))
# construct the head of the model that will be placed on top of the
# the base model
headModel = baseModel.output
headModel = AveragePooling2D(pool_size=(7, 7))(headModel)
headModel = Flatten(name="flatten")(headModel)
headModel = Dense(256, activation="relu")(headModel)
headModel = Dropout(0.5)(headModel)
headModel = Dense(len(CLASSES), activation="softmax")(headModel)
# place the head FC model on top of the base model (this will become
# the actual model we will train)
model = Model(inputs=baseModel.input, outputs=headModel)
# loop over all layers in the base model and freeze them so they will
# *not* be updated during the training process
for layer in baseModel.layers:
	layer.trainable = False
    

# compile the model
opt = Adam(lr=INIT_LR, decay=INIT_LR / args.epochs)
model.compile(loss="binary_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# train the model
print("[INFO] training model...")
H = model.fit_generator(
	trainGen,
	steps_per_epoch=totalTrain // args.batchsize,
	validation_data=valGen,
	validation_steps=totalVal // args.batchsize,
	epochs=args.epochs)

# reset the testing generator and then use our trained model to
# make predictions on the data
print("[INFO] evaluating network...")
testGen.reset()
predIdxs = model.predict_generator(testGen,
	steps=(totalTest // args.batchsize) + 1)
# for each image in the testing set we need to find the index of the
# label with corresponding largest predicted probability
predIdxs = np.argmax(predIdxs, axis=1)
# show a nicely formatted classification report
print(classification_report(testGen.classes, predIdxs,
	target_names=testGen.class_indices.keys()))
# serialize the model to disk
print("[INFO] saving model...")
fsave_model = "./FineTuningResNet50_cdt{}_{}_ep{}_bs{}_it{}_model.h5".format(
    args.condition, args.weights, args.epochs, args.batchsize, args.iteration) 
model.save(fsave_model)

fsave_plot = "./FineTuningResNet50_cdt{}_{}_ep{}_bs{}_it{}_model.png".format(
    args.condition, args.weights, args.epochs, args.batchsize, args.iteration) 

# plot the training loss and accuracy
N = args.epochs
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy on Dataset")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(fsave_plot)